import React from 'react';

interface Props
{
    width?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class DatatableHeadCell extends React.Component<Props,State>
{
    render()
    {
        const style = { };
        if(this.props.width)
            style['width'] = this.props.width;
        return (
<div className="datatable-head-cell" style={style}>
    { this.props.children }
</div>
        );
    }
};
export default DatatableHeadCell;
