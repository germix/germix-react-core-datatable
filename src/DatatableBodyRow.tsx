import React from 'react';

interface Props
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class DatatableBodyRow extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="datatable-body-row">
    { this.props.children }
</div>
        );
    }
};
export default DatatableBodyRow;
