import React from 'react';

interface Props
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class DatatableHead extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="datatable-head">
    <div className="datatable-head-row">
        { this.props.children }
    </div>
</div>
        );
    }
};
export default DatatableHead;
