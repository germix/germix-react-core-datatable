import React from 'react';

interface Props
{
    align? : 'left'|'center'|'right',
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Datatable extends React.Component<Props,State>
{
    render()
    {
        let className = ['datatable'];
        if(this.props.align)
        {
            className.push('datatable-align-' + this.props.align);
        }
        return (
<div className={className.join(' ')}>
    { this.props.children }
</div>
        );
    }
};
export default Datatable;
