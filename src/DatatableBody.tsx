import React from 'react';

interface Props
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class DatatableBody extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="datatable-body">
    { this.props.children }
</div>
        );
    }
};
export default DatatableBody;
