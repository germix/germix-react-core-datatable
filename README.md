# Germix React Core - Datatable

## About

Germix react core datatable components

## Installation

```bash
npm install @germix/germix-react-core-datatable
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
